@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="text-center"> FACTURAS</h2>
                <form action="{{route ('facturas.generar')}}" method="POST">
                    @csrf
                <button type="submit" class="btn btn-primary"> Generar Facturas</button>
                <a class="btn btn-warning" href="/producto/ ">Ir a Productos</a>

                </form>
                <div class="card">


                    <table class="table thead-dark table-striped">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">Cliente</th>
                            <th scope="col">Costo total de productos</th>
                            <th scope="col">Total de Impuesto %</th>
                            <th scope="col" colspan="2">Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($facturas as $factura)

                            <tr>
                                <td>{{$factura->name}}</td>
                                <td>{{number_format($factura->total_compra, 2, '.', '').' $'}}</td>
                                <td>{{$factura->total_impuesto}}</td>


                                <td>
                                    <a class="btn btn-warning" href="/facturas/{{ $factura->id}}/show ">VER DETALLE DE FACTURA</a>

                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
@endsection