@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="text-center"> FACTURAS</h2>
                <div>
                    <a href="/facturas" class="btn btn-primary"> Atras</a>
                </div>
                <div class="card">
                    <table class="table thead-dark table-striped">
                        <thead class="table-dark">
                        <tr>

                            <th scope="col">Nombre del Producto</th>
                            <th scope="col">Precio del producto $</th>
                            <th scope="col">Impuesto del producto %</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3"><b>Cliente:</b> {{$montos->name}}</td>
                        </tr>
                        @foreach($facturas as $factura)

                            <tr>
                                <td>{{$factura->nombre}}</td>
                                <td>{{number_format($factura->precio, 2, '.', '')}}</td>
                                <td>{{$factura->impuesto}}</td>
                            </tr>
                        @endforeach
<tr>

    <td></td>
    <td>Monto Total: {{number_format($montos->total_compra, 2, '.', '').' $'}}</td>
    <td>Impuesto Total: {{$montos->total_impuesto}}</td>

</tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

@endsection