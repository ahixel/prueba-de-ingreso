@extends('layouts.app')

@section('content')
    <h2 class="text-center"> REALIZAR COMPRA</h2>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="/compra" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label for="" class="form-label">Producto</label>
                        <select class="form-control" name="producto_id" id="producto_id">
                            <option value="">Seleccione producto</option>
                            @foreach($productos as $producto)
                                <option value="{{ $producto->id }}">{{ $producto->nombre .' - '. $producto->precio.' $ '}}</option>
                        @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success" tabindex="4"> Guardar </button>

                </form>
            </div>
        </div>
    </div>
@endsection