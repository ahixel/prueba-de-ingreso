@extends('layouts.app')

@section('content')
    <h2 class="text-center"> CREAR PRODUCTO</h2>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="/producto" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label for="" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Precio</label>
                        <input type="text" class="form-control" name="precio" id="precio">
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Impuesto %</label>
                        <input type="text" class="form-control" name="impuesto" id="impuesto">
                    </div>

                    <a href="/producto" class="btn btn-secondary" tabindex="5"> Cancelar</a>
                    <button type="submit" class="btn btn-success" tabindex="4"> Guardar </button>

                </form>
            </div>
        </div>
    </div>
@endsection