@extends('layouts.app')

@section('content')
    <h2 class="text-center"> ACTUALIZAR REGISTRO</h2>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="/producto/{{$producto->id}}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="" class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{$producto->nombre}}">
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Precio</label>
                        <input type="text" class="form-control" name="precio" id="precio" value="{{$producto->precio}}">
                    </div>

                    <div class="mb-3">
                        <label for="" class="form-label">Impuesto %</label>
                        <input type="text" class="form-control" name="impuesto" id="impuesto" value="{{$producto->impuesto}}">
                    </div>

                    <a href="/producto" class="btn btn-secondary" tabindex="5"> Cancelar</a>
                    <button type="submit" class="btn btn-success" tabindex="4"> Guardar </button>

                </form>
            </div>
        </div>
    </div>
@endsection