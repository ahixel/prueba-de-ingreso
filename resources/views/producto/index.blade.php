@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="text-center"> PRODUCTOS</h2>
                <a href="{{route ('producto.create')}}" class="btn btn-primary"> Crear</a>
                <a class="btn btn-warning " href="/facturas/ ">Ir a Facturas</a>
                <div class="card">


                    <table class="table thead-dark table-striped">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio(impuesto incluido) $ </th>
                            <th scope="col">Impuesto %</th>
                            <th scope="col" colspan="2">Acciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($response as $resultado)

                            <tr>
                                <td>{{$resultado->nombre}}</td>
                                <td>{{number_format($resultado->precio, 2, '.', ''). ' $'}}</td>
                                <td>{{$resultado->impuesto}}</td>
                                <form action="{{route ('producto.delete', $resultado->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                <td>
                                    <a class="btn btn-warning" href="producto/{{ $resultado->id}}/edit ">Actualizar</a>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-danger">Eliminar</button>

                                </td>
                                </form>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

@endsection