<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

//Route::get('/', [App\Http\Controllers\ProductoController::class, 'index']);

///PRODUCTOS///
Route::get('/producto', [App\Http\Controllers\ProductoController::class, 'index'])->name('producto');
Route::get('/producto/create', [App\Http\Controllers\ProductoController::class, 'create'])->name('producto.create');
Route::post('/producto', [App\Http\Controllers\ProductoController::class, 'store'])->name('producto.store');
Route::get('/producto/{producto}/edit', [App\Http\Controllers\ProductoController::class, 'edit'])->name('producto.edit');
Route::put('/producto/{producto}', [App\Http\Controllers\ProductoController::class, 'update'])->name('producto.update');
Route::delete('/producto/{producto}', [App\Http\Controllers\ProductoController::class, 'destroy'])->name('producto.delete');

////////COMPRAS
Route::get('/compra', [App\Http\Controllers\CompraController::class, 'create'])->name('compra.create');
Route::post('/compra', [App\Http\Controllers\CompraController::class, 'store'])->name('compra.store');

///////FACTURAS
Route::get('/facturas', [App\Http\Controllers\FacturaController::class, 'index'])->name('facturas.index');
Route::post('/facturas-generar', [App\Http\Controllers\FacturaController::class, 'generar'])->name('facturas.generar');
Route::get('/facturas/{id}/show', [App\Http\Controllers\FacturaController::class, 'show'])->name('facturas.show');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
