<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function producto()
    {
        return $this->hasMany('App\Models\Producto', 'id', 'producto_id');
    }
}
