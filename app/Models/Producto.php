<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function compra()
    {
        return $this->belongsTo('App\Models\Compra', 'id', 'producto_id');
    }

}
