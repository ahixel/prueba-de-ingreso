<?php

namespace App\Http\Controllers;

use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Http;
use App\Models\Producto;


class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Producto::all();
        return view('producto.index')->with("response", $response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('producto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validated= $this->validate($request,[
            'nombre' => 'required|min:3',
            'precio' => 'required|numeric',
            'impuesto' => 'required|numeric',
        ]);

        $arrayInsert = [
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'impuesto' => $request->impuesto,
        ];

        $response = Producto::create($arrayInsert);
        return redirect('/producto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {

        return view('producto.edit')->with('producto', $producto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::findOrFail($id);
        $data = $request->all();
        $producto->update($data);
        return redirect('/producto');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {

        $producto = Producto::where('id', $producto->id)->first();
        if ($producto){
            $producto->delete();
        }
        return redirect('/producto');
    }
}
