<?php

namespace App\Http\Controllers;

use App\Models\Compra;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Http;
use App\Models\Producto;
use App\Models\Factura;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas =  Factura::select('facturas.id', 'facturas.user_id', 'facturas.total_compra', 'facturas.total_impuesto'
            , 'users.name')
            ->join('users', 'facturas.user_id', '=', 'users.id')->get();

           // dd($facturas);

        return view('facturas.index')->with("facturas", $facturas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $factura_id=0;
        $facturas =  Factura::select('facturas.id', 'facturas.user_id', 'facturas.compras','facturas.total_compra', 'facturas.total_impuesto',
            'users.id', 'users.name')
            ->join('users', 'facturas.user_id', '=', 'users.id')->where('facturas.id',$id)->first();
       
        $compras_array=explode(",", $facturas->compras);

        $compras = Compra::select('compras.id', 'compras.user_id', 'compras.producto_id',
            'productos.nombre', 'productos.precio', 'productos.impuesto', 'compras.created_at','users.name')
            ->join('productos', 'compras.producto_id', '=', 'productos.id')
            ->join('users', 'users.id', '=', 'compras.user_id')
            ->whereIn('compras.id',$compras_array)->orderBy('compras.created_at')->get();
  
    
    //print_r($compras);exit;

        return view('facturas.detalle_factura')->with("facturas", $compras)->with('montos',$facturas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {

    }

    public function generar()
    {
        $id_compras = array();

            $compras = Compra::select('compras.id', 'compras.user_id', 'compras.producto_id',
                'productos.nombre', 'productos.precio', 'productos.impuesto', 'compras.created_at')
                ->join('productos', 'compras.producto_id', '=', 'productos.id')
                ->where('compras.facturado',0)
                ->orderBy('compras.user_id')
                ->get();

                $arrayInsert=[];

            foreach ($compras as $compra) {
                $id_user=$compra->user_id;
                $id_compras[] =$compra->id;
                $compra_update=Compra::find($compra->id);
                $compra_update->facturado=1;
                $compra_update->save();
                $repeat=false;
                for($i=0;$i<count($arrayInsert);$i++){
                    
                    if($arrayInsert[$i]['user_id']==$id_user)
                    {
                        $arrayInsert[$i]['total_compra']+=$compra->precio;
                        $arrayInsert[$i]['total_impuesto']+=$compra->impuesto;
                        $arrayInsert[$i]['compras'] .= ','.$compra->id;
                        $repeat=true;
                        break;
                    }
                }
                if($repeat==false)
                    $arrayInsert[] = [
                        'compras' => $compra->id,
                        'user_id' => $id_user,
                        'total_compra' => $compra['precio'],
                        'total_impuesto' => $compra['impuesto'],
                    ];

            }
          
            foreach($arrayInsert as $insert){

                $response = Factura::create($insert);    
            }

            return redirect('facturas');

            
    }
}
